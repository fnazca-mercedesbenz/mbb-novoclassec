$(document).on('ready', () => {

    $(window).on('close:menu', toogleMenu);
    $(window).on('slide:hotspots', loadSlideHotspots);
    $(window).on('start:slides', () => {

        $('.models__container__content-mobile .models__container__content__sedan').slick({
            slidesToShow: 1,
            slidesToScroll: 1,
            centerMode: false,
            infinite: true,
            arrows: false,
            cssEase: 'linear',
            variableWidth: false,
            swipe: true,
            arrows: true,
        });

        $('.models__container__content-mobile .models__container__content__coupe').slick({
            slidesToShow: 1,
            slidesToScroll: 1,
            centerMode: false,
            infinite: true,
            arrows: false,
            cssEase: 'linear',
            variableWidth: false,
            swipe: true,
            arrows: true,
        });

        $('.models__container__content-mobile .models__container__content__cabriolet').slick({
            slidesToShow: 1,
            slidesToScroll: 1,
            centerMode: false,
            infinite: true,
            arrows: false,
            cssEase: 'linear',
            variableWidth: false,
            swipe: true,
            arrows: true,
        });
    });

    $('.menu-bar').on('click', () => {
       toogleMenu();
    });

    function toogleMenu() {
        const menuVisible = $('.nav-head__menu--mobile').is(':visible');
        
        $('.menu-bar').toggleClass('actived');

        if (menuVisible) {
            $('body').removeAttr('style');
            $('.nav-head__menu--mobile').fadeOut();
            $('.nav-head').animate({
                height: '0'
            }, 400);
            
        } else {
            $('body').css('overflow', 'hidden');
            $('.nav-head').animate({
                height: '110vh'
            }, 400, () => {
                $('.nav-head__menu--mobile').fadeIn();
            });
        }
    }

    function loadSlideHotspots() {
        $('.hotspot .hotspot__container__external--mobile').slick({
            slidesToShow: 1,
            slidesToScroll: 1,
            centerMode: false,
            infinite: true,
            arrows: false,
            dots: true,
            cssEase: 'linear',
            variableWidth: false,
            adaptiveHeight: true,
        });

        $('.hotspot .hotspot__container__internal--mobile').slick({
            slidesToShow: 1,
            slidesToScroll: 1,
            centerMode: false,
            infinite: true,
            arrows: false,
            dots: true,
            cssEase: 'linear',
            variableWidth: false,
            adaptiveHeight: true,
        });


        $('.hotspot__container .mobile-slide-content').slick({
            slidesToShow: 1,
            slidesToScroll: 1,
            centerMode: false,
            infinite: true,
            arrows: false,
            cssEase: 'linear',
            variableWidth: true,
            swipe: false,
            arrows: true,
          });
    };


    $('.zoom.mobile').on('click', () => {
        const id = $('.gallery__container.hide-desktop .slick-active').find('img').attr('id');

        if($(window).width() > 1023) {
            return;
        }
        
        $('.gallery .zoom.mobile').toggleClass('active');

        if ($('.gallery__container__item img').hasClass('actived')) {
            $('#Boilerplate').css('opacity', 1);
        } else {
            $('#Boilerplate').css('opacity', 0);
        }
        
        $('.gallery__container__item img').toggleClass('actived');

        if (!window.zoom) {
          $(`#${id}`).elevateZoom({
            zoomType: "inner",
            cursor: "crosshair",
            easing: false,
            imageCrossfade: false, 
            onZoomedImageLoaded: function() {
                $('#Boilerplate').css('opacity', 1);
                $('.zoomWindow').addClass('visible');
            }
          });
          
          window.zoom = true;
          $(`#${id}`).bind('touchmove');
        }
        else {
          $(`#${id}`).removeData('elevateZoom').off('touchmove');
          $('.zoomContainer').remove();
          window.zoom = false;
        }
    });

    $('.features__container .slide').slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        infinite: true,
        arrows: false,
        cssEase: 'linear',
        swipe: true,
        dots: true,
        adaptiveHeight: true,
      });


      
        $(`.hotspot__container__internal--mobile, .hotspot__container__internal--mobile .mobile-slide-content`).on('beforeChange', (slick) => {
            $('.hotspot__container__internal--mobile .mobile-slide-content .slick-slide .text-row').removeAttr('style');
            $('.hotspot__container__internal--mobile .mobile-slide-content .slick-slide .text-desc').removeAttr('style');
        });


        $(`.hotspot__container__external--mobile, .hotspot__container__external--mobile .mobile-slide-content`).on('beforeChange', (slick) => {
            $('.hotspot__container__external--mobile .mobile-slide-content .slick-slide .text-row').removeAttr('style');
            $('.hotspot__container__external--mobile .mobile-slide-content .slick-slide .text-desc').removeAttr('style');
        });

        $(`.hotspot__container__external--mobile, .hotspot__container__external--mobile .mobile-slide-content`).on('afterChange', (slick) => {
            const index = $('.hotspot__container__external--mobile .mobile-slide-content').slick('slickCurrentSlide');
            const indexParent = $('.hotspot__container__external--mobile').slick('slickCurrentSlide');

            if (indexParent > 0) {
                $(`.hotspot__container__external--mobile .mobile-slide-content`).slick('slickGoTo', 0);
            }
            $('.hotspot__container__external--mobile .mobile-slide-content .slick-slide .text-row').removeAttr('style');
            $('.hotspot__container__external--mobile .mobile-slide-content .slick-slide .text-desc').removeAttr('style');

            $('.hotspot__container__external--mobile .mobile-slide-content .slick-slide[data-slick-index="' + index + '"] .text-desc')
             .css('opacity', 1);

            $('.hotspot__container__external--mobile .mobile-slide-content .slick-slide[data-slick-index="' + index + '"] .text-row')
                .css('opacity', 1);
        });

        $(`.hotspot__container__internal--mobile, .hotspot__container__internal--mobile .mobile-slide-content`).on('afterChange', (slick) => {
            const index = $('.hotspot__container__internal--mobile .mobile-slide-content').slick('slickCurrentSlide');
            const indexParent = $('.hotspot__container__internal--mobile').slick('slickCurrentSlide');

            if (indexParent > 0) {
                $(`.hotspot__container__internal--mobile .mobile-slide-content`).slick('slickGoTo', 0);
            }
            $('.hotspot__container__internal--mobile .mobile-slide-content .slick-slide .text-row').removeAttr('style');
            $('.hotspot__container__internal--mobile .mobile-slide-content .slick-slide .text-desc').removeAttr('style');

            $('.hotspot__container__internal--mobile .mobile-slide-content .slick-slide[data-slick-index="' + index + '"] .text-row')
                .css('opacity', 1);
            
            $('.hotspot__container__internal--mobile .mobile-slide-content .slick-slide[data-slick-index="' + index + '"] .text-desc')
                .css('opacity', 1);
        });

});