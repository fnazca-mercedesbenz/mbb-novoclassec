window.mercedes_states = 
{
	/**
	 * Triggered event on before get states
	 */
	onBeforeSend: function()
	{
		console.log('Carregando estados...');
	},

	/**
	 * Triggered event on get data success
	 */
	onGetSuccess: function(p_response)
	{
		console.log('Pronto.');
		for(var uf in p_response)
		{
			$('#uf').append($('<option>', { value: uf, text: p_response[uf] }));
		}
	},

	/**
	 * Triggered event on get data error
	 */
	onGetMessage: function(p_message)
	{
		console.log('Mercedes-Benz', p_message, 'error');
	},

	/**
	 * Triggered event on request error
	 */
	onGetFail: function()
	{
		console.log('Mercedes-Benz', 'Ocorreu um erro na solicitação.', 'error');
	},

	/**
	 * Triggered event on aways request
	 */
	onGetAlways: function()
	{
		
	},
};

window.mercedes_dealers = 
{
	/**
	 * Triggered event on before get dealers
	 */
	onBeforeSend: function()
	{
		console.log('Carregando concessionárias...');
	},

	/**
	 * Triggered event on get data success
	 */
	onGetSuccess: function(p_response, p_cached)
	{
		$('#dealer option:not(:first)').remove();
		$('#dealer option:first').prop('selected', true);

		var dealer;
		for(var k in p_response)
		{
			dealer = p_response[k];
			$('#dealer').append($('<option>', { value: dealer.id, text: dealer.name }));
		}

		if (p_cached === undefined)
		{
			console.log('Pronto.');
		}
	},

	/**
	 * Triggered event on get data error
	 */
	onGetMessage: function(p_message)
	{
		console.log('Mercedes-Benz', p_message, 'error');
	},

	/**
	 * Triggered event on request error
	 */
	onGetFail: function()
	{
		console.log('Mercedes-Benz', 'Ocorreu um erro na solicitação.', 'error');
		$('.form .message-error').fadeIn();
	},

	/**
	 * Triggered event on aways request
	 */
	onGetAlways: function()
	{
		
	},
};

window.mercedes_leads = 
{
	/**
	 * Gets url API
	 * @return {String}
	 */
	getUrlApi: function()
	{
		var host = window.location.href

		if (host.indexOf('hml37') > -1) {
			return "https://mbb-leads-api-hml.websiteseguro.com/api";
		  }
		
		  if (host.indexOf('novoclassec') > -1) {
			return "https://mbb-leads-api.websiteseguro.com/api";
		  }
		
		  if (host.indexOf('localhost') > -1 || host.indexOf('umstudio') > -1) {
			return  "https://mbb-leads-api-hml.websiteseguro.com/api";
		  }

		// PRODUCTION
		// return 'http://mbb-leads-api.servicos.ws/api';
	},

	/**
	 * Gets main form
	 * @return {jQuery form element}
	 */
	getForm: function()
	{
		return $('#lead');
	},

	/**
	 * Gets custom data form object
	 * @return {Data form Object}
	 */
	getData: function()
	{
		var result = 
		{
			'subject'          : $('#lead [name="form_assunto"]').val(),
			'uf'               : $('#lead [name="form_estado"]').val(),
			'dealer'           : $('#lead [name="form_concessionaria"]').val(),
			'utm_source'       : $('#lead [name="utm_source"]').val(),
			'utm_medium'       : $('#lead [name="utm_medium"]').val(),
			'utm_campaign'     : $('#lead [name="utm_campaign"]').val(),
			'utm_content'      : $('#lead [name="utm_content"]').val(),
			'car'	           : $('#lead [name="form_car"]').val(),
			'name'             : $('#lead [name="form_nome"]').val(),
			'phone'            : $('#lead [name="form_telefone"]').val(),
			'email'            : $('#lead [name="form_email"]').val(),
			'comment'          : $('#lead [name="form_comentario"]').val() ? $('#lead [name="form_comentario"]').val() : 'sem comentários',
			'captcha_response' : $('#lead [name="g-recaptcha-response"]').val()
		};
		return result;
	},

	/**
	 * Triggered event on before send form
	 */
	onBeforeSend: function()
	{
		console.log('Enviando dados...');
		$('.container-form').addClass('form-loading');
	},

	/**
	 * Triggered event on form save success
	 */
	onSaveSuccess: function(p_response)
	{
		console.log('Mercedes-Benz', p_response, 'success');
		$('.form-loading').hide();
		$('.form .success').show();
		/*dataLayer.push({
			tipo: 'Evento',
			category: 'classe-c-facelift',
			action: 'formulario',
			label: 'enviar',
		});*/

		ga('send', 'event', 'classe-c-facelift', 'formulario', 'enviar');
	},

	/**
	 * Triggered event on save form error
	 */
	onSaveMessage: function(p_response)
	{
		console.log('Mercedes-Benz', p_response, 'error');
		$('.message-error').show();
		grecaptcha.reset();
	},

	/**
	 * Triggered event on save form fail
	 */
	onSaveFail: function(message_error)
	{
		if (!message_error)
		{
			console.log('Mercedes-Benz', 'Ocorreu um erro na solicitação.', 'error');
			$('.form .message-error').show();
		}
		else
		{
			console.log('Mercedes-Benz', 'Ocorreu um erro na solicitação: ' + message_error, 'error');
			$('.form .message-error').show();
		}

		grecaptcha.reset();
		$('.form-loading').hide();
	},

	/**
	 * Triggered event always after save form send
	 */
	onSaveAlways: function()
	{
		$('.container-form').removeClass('form-loading');
	}
};