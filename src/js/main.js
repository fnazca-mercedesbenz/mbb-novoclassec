$(document).on('ready', function() {
  window.zoom = false;
  let host = window.location.href;
  let dataModels;
  let modalActive;
  let formActiveOnLoad = $(window).width() <= 1023 ? false : true;
  window.lat;
  window.lng;
  window.jeep = {};
  window.jeep.hotspots = null;
  window.currentFeature = 'internal';
  let blockAutoClass = false;
  let mapLoaded = false;
  let lockScroll = true;
  
  if (window.location.hash) {
    if ($(window.location.hash).length <= 0) return;
    const target = $(window.location.hash).attr('class').replace(' no-padding', '').trim();
    setTimeout(() => {
        scrollTo(target);
    }, 2000);
  }

  window.onhashchange = changeHash;
  function changeHash() 
  {
    if(lockScroll)
    {
      scrollTo($(window.location.hash).attr('class').replace(' no-padding', '').trim());
    }
  }

  
  if (host.indexOf('hml37') > -1) {
    $.getScript( "https://mbb-leads-api-hml.websiteseguro.com/api/mercedes_send_lead.min.js" );
  }

  if (host.indexOf('novoclassec') > -1) {
    $.getScript( "https://mbb-leads-api.websiteseguro.com/api/mercedes_send_lead.min.js" );
  }

  if (host.indexOf('localhost') > -1 || host.indexOf('umstudio') > -1) {
    $.getScript( "https://mbb-leads-api-hml.websiteseguro.com/api/mercedes_send_lead.min.js" );
  }

  if (Url.queryString('utm_source')  ) { Cookies.set('utm_source'  , Url.queryString('utm_source')  ); }
  if (Url.queryString('utm_medium')  ) { Cookies.set('utm_medium'  , Url.queryString('utm_medium')  ); }
  if (Url.queryString('utm_content') ) { Cookies.set('utm_content' , Url.queryString('utm_content') ); }
  if (Url.queryString('utm_campaign')) { Cookies.set('utm_campaign', Url.queryString('utm_campaign')); }

  $('input[name="utm_source"]'  ).val(Cookies.get('utm_source')  );
  $('input[name="utm_medium"]'  ).val(Cookies.get('utm_medium')  );
  $('input[name="utm_campaign"]').val(Cookies.get('utm_content') );
  $('input[name="utm_content"]' ).val(Cookies.get('utm_campaign'));

  if(!formActiveOnLoad) {
    $('.form').removeClass('actived');
  } else {
    $('.form').addClass('actived');
  }

  $.ajax({
    url: "./js/models.json",
    dataType: 'json',
  })
  .then((response) => {
    dataModels = response.models;

    dataModels.map((model) => {
      loadModels(model);
    });

    $(window).trigger('start:slides');
  });

  $.ajax({
    url: "./js/hotspots.json",
    dataType: 'json',
  }).then((result) => {
    window.jeep.hotspots = result;
    
    $(window).trigger('slide:hotspots');
  });

  $('[data-scroll]').on('click', (e) => {
    var id = $(e.currentTarget).data('scroll');
    blockAutoClass = true;
    lockScroll = false;
    history.pushState(null, null, '#' + $(e.currentTarget).attr('data-anchor'));
    $('.nav-head__menu li').removeClass('active');
    $('.nav-head__menu').find('li[data-scroll="'+ id +'"]').addClass('active');

    const target = $(e.currentTarget).data('scroll');
    scrollTo(target);
  });


  $('.form .success button').on('click', () => {
    $('#lead')[0].reset();
    $('#lead select[name="form_estado"]').data('selectric').refresh();
    $('#lead select[name="form_assunto"]').data('selectric').refresh();
    $('#lead select[name="form_cidade"]').data('selectric').refresh();
    $('#lead select[name="form_concessionaria"]').data('selectric').refresh();
    grecaptcha.reset();
    $('.form .success').fadeOut();
    $('#lead').show();
    $('.form').toggleClass('actived');
  });

  $('.form .message-error button').on('click', () => {
    $('.form .message-error').fadeOut();
    $('#lead').show();
  });


  var SPMaskBehavior = function (val) {
    return val.replace(/\D/g, '').length === 11 ? '(00) 00000-0000' : '(00) 0000-00009';
  },
  spOptions = {
    onKeyPress: function(val, e, field, options) {
        field.mask(SPMaskBehavior.apply({}, arguments), options);
      }
  };
  
  $('#form_telefone').mask(SPMaskBehavior, spOptions);

    // cache the navigation links 
    var $navigationLinks = $('.nav-head__menu li');
    // cache (in reversed order) the sections
    var $sections = $($("section").get().reverse());

    // map each section id to their corresponding navigation link
    var sectionIdTonavigationLink = {};
    $sections.each(function() {
        var id = $(this).attr('class').replace(' no-padding', '').trim();
        //console.log($(`.nav-head__menu li[data-scroll="${id}"]`));
        sectionIdTonavigationLink[id] = $(`.nav-head__menu li[data-scroll="${id}"]`);
    });

    // throttle function, enforces a minimum time interval
    function throttle(fn, interval) {
        var lastCall, timeoutId;
        return function () {
            var now = new Date().getTime();
            if (lastCall && now < (lastCall + interval) ) {
                // if we are inside the interval we wait
                clearTimeout(timeoutId);
                timeoutId = setTimeout(function () {
                    lastCall = now;
                    fn.call();
                }, interval - (now - lastCall) );
            } else {
                // otherwise, we directly call the function 
                lastCall = now;
                fn.call();
            }
        };
    }

    function highlightNavigation() {
        // get the current vertical position of the scroll bar
        var scrollPosition = $(window).scrollTop();

        // iterate the sections
        $sections.each(function() {
            var currentSection = $(this);
            // get the position of the section
            var sectionTop = currentSection.offset().top;

            // if the user has scrolled over the top of the section  
            if (scrollPosition >= sectionTop - 100 && !blockAutoClass) {
                // get the section id
                var id = currentSection.attr('class').replace(' no-padding', '').trim();
                // get the corresponding navigation link
                var $navigationLink = sectionIdTonavigationLink[id];
                // if the link is not active
                if (id === 'maps' && !mapLoaded || id === 'models' && !mapLoaded) {
                    mapLoaded = true;
                    initMap();
                }

                if(!$navigationLink) {
                    return;
                }
                
                if (!$navigationLink.hasClass('active')) {
                    // remove .active class from all the links
                    $navigationLinks.removeClass('active');
                    // add .active class to the current link
                    $navigationLink.addClass('active');
                    history.pushState(null, null, '#' + currentSection.attr('id'));
                }
                // we have found our section, so we return false to exit the each loop
                return false;
            }
        });
    }

    $(window).scroll( throttle(highlightNavigation,100) );

    function scrollTo(target) {
        if ($('.nav-head__menu--mobile').is(':visible')) {
            $(window).trigger('close:menu');
        }

        let $target = $(`.${target}`).offset().top - 60;

        if (target === 'highlight') {
            $target = 0;
        }
        //console.log(target);
        $('html, body').animate({
            scrollTop: $target,
        }, 1000, 'easeOutCubic');

        setTimeout(() => {
            blockAutoClass = false;
        }, 1001);

        setTimeout(() => {
            lockScroll = true;
        }, 1500);
    }

    $('#lead .highlight').on('click', (e) => {
        e.preventDefault();

        $('body').css('overflow', 'hidden');
        
        $('#privacy').fadeIn();
        $(".style-scroll").mCustomScrollbar({
            style: "light",
        });
    });

    $('.info-privacy a').on('click', () => {
        $('body, html').removeAttr('style');
        $('#privacy').fadeOut();
    });

  /***
 *      _  _   ___  _____  ___  ___   ___  _____ 
 *     | || | / _ \|_   _|/ __|| _ \ / _ \|_   _|
 *     | __ || (_) | | |  \__ \|  _/| (_) | | |  
 *     |_||_| \___/  |_|  |___/|_|   \___/  |_|  
 *                                               
 */

  setTimeout(() => {
    $('.hotspot__container__internal').hotSpot({

        // default selectors
        mainselector: '.hotspot__container__internal',
        selector: '.hotspot__container__internal .hotspot-item',
        imageselector: '.img-responsive',
        tooltipselector: '.tooltip',
      
        // or 'click'
        bindselector: 'click'
        
      });
    
      $('.hotspot__container__external').hotSpot({
    
        // default selectors
        mainselector: '.hotspot__container__external',
        selector: '.hotspot__container__external .hotspot-item',
        imageselector: '.img-responsive',
        
      });
  }, 1000);

  $('.hotspot [data-item]').on('click', (e) => {
    if ( !$(e.currentTarget).find('.tooltip').is(':visible')) {
        loadHotspotItem(e); 
    }
  });

  $(document).on('click', '.tooltip .close', () => {
    closeHotspot();
  });


  $('[data-hotspot]').on('click',  (e) => {
    const target = $(e.currentTarget).data('hotspot');

    window.currentFeature = target;

    closeHotspot();

    /*$('.hotspot .title').hide();*/

    if (target === 'internal') {
      $('.hotspot__container__external, .hotspot__container__external--mobile').hide();
    } else {
      $('.hotspot__container__internal, .hotspot__container__internal--mobile').hide();
    }

    $('[data-hotspot]').removeClass('actived');
    $(e.currentTarget).addClass('actived');
    $(`.hotspot__container__${target}`).show();
    
    /*$(`.hotspot__container__${target}`).fadeIn(() => {
        $('.hotspot .title').fadeIn();
    });*/
    
    $(`.hotspot__container__${target}--mobile`).fadeIn(() => {
        setTimeout(() => {
            $(`.hotspot__container__${target}--mobile`).slick('setPosition');
            $(`.hotspot__container__${target}--mobile .mobile-slide-content`).slick('setPosition');
        })
    });

    setTimeout(() => {
        $('.hotspot__container__internal').hotSpot({

            // default selectors
            mainselector: '.hotspot__container__internal',
            selector: '.hotspot__container__internal .hotspot-item',
            imageselector: '.img-responsive',
            tooltipselector: '.tooltip',
          
            // or 'click'
            bindselector: 'click'
            
          });
        
          $('.hotspot__container__external').hotSpot({
        
            // default selectors
            mainselector: '.hotspot__container__external',
            selector: '.hotspot__container__external .hotspot-item',
            imageselector: '.img-responsive',
            
          });
    })
  });

  function loadHotspotItem(e) {
    const currentHotspot = $(e.currentTarget).find('.tooltip');

    scrollTo('hotspot');
    $('.tooltip:not(.mobile-slide__item)').fadeOut();

    setTimeout(() => {
        $(currentHotspot).fadeIn();
        $(currentHotspot).find('.slide-content').slick({
            slidesToShow: 1,
            slidesToScroll: 1,
            centerMode: false,
            infinite: true,
            arrows: true,
            cssEase: 'linear',
            variableWidth: true,
        });
    }, 400);
  };

  function closeHotspot() {
    $('.tooltip:not(.mobile-slide__item)').fadeOut();
  }

  $(`.hotspot__container__internal`).on('beforeChange', (slick) => {
    $('.hotspot__container__internal .slick-slide .text-row').removeAttr('style');
    $('.hotspot__container__internal .slick-slide .text-desc').removeAttr('style');
  });


  $(`.hotspot__container__external`).on('beforeChange', (slick) => {
    $('.hotspot__container__external .slick-slide .text-row').removeAttr('style');
    $('.hotspot__container__external .slick-slide .text-desc').removeAttr('style');
  });

  $(`.hotspot__container__external`).on('afterChange', (slick) => {
    const index = $('.hotspot__container__external .slide-content').slick('slickCurrentSlide');
    $('.hotspot__container__external .slick-slide .text-row').removeAttr('style');
    $('.hotspot__container__external .slick-slide .text-desc').removeAttr('style');

    $('.hotspot__container__external .slick-slide[data-slick-index="' + index + '"] .text-row')
      .css('opacity', 1);
  });

  $(`.hotspot__container__internal`).on('afterChange', (slick) => {
    const index = $('.hotspot__container__internal .slide-content').slick('slickCurrentSlide');
    
    $('.hotspot__container__internal .slick-slide[data-slick-index="' + index + '"] .text-row')
      .css('opacity', 1);
  });


  /***
 *       ___    _    _     _     ___  ___ __   __
 *      / __|  /_\  | |   | |   | __|| _ \\ \ / /
 *     | (_ | / _ \ | |__ | |__ | _| |   / \ V / 
 *      \___|/_/ \_\|____||____||___||_|_\  |_|  
 *                                               
 */
  $('.gallery__container').slick({
    slidesToShow: 1,
    slidesToScroll: 1,
    arrows: false,
    dots: true,
    arrows: true,
    cssEase: 'cubic-bezier(.25,.48,.56,.74)',
  });

  $(document).on('click', '.gallery .zoom, .zoomContainer', () => {
    const id = $('.gallery__container.hide-mobile .slick-active').find('img').attr('id');

    if ( $(window).width() <= 1023) {
      return;
    }

    $('.gallery .zoom:not(.mobile)').toggleClass('active');

    if (!window.zoom) {
      $(`#${id}`).elevateZoom({
        zoomType: "inner",
        cursor: "zoom-out",
        easing: true,
      });
      setTimeout(() => {
        $('.zoomWindow').addClass('visible');
      }, 100);
      
      window.zoom = true;
    }
    else {
      $(`#${id}`).removeData('elevateZoom');
      $('.zoomContainer').remove();
      window.zoom = false;
    }
  });

  $(window).resize(function(e){
    if (!window.zoom) {
      return;
    }

    setTimeout( () => {
      $('.zoomContainer').remove();
    }, 100);
  }); 


  /***
   *      __  __   ___   ___   ___  _     ___ 
   *     |  \/  | / _ \ |   \ | __|| |   / __|
   *     | |\/| || (_) || |) || _| | |__ \__ \
   *     |_|  |_| \___/ |___/ |___||____||___/
   *                                          
   */


  $('[data-models]').on('click',  (e) => {
    const target = $(e.currentTarget).data('models');

    closeModal();

    if (target === 'sedan') {
      $(`.models__container__content__coupe`).hide();
      $(`.models__container__content__cabriolet`).hide();
    } 

    if (target === 'coupe') {
      $(`.models__container__content__sedan`).hide();
      $(`.models__container__content__cabriolet`).hide();
    }

    if (target === 'cabriolet') {
      $(`.models__container__content__sedan`).hide();
      $(`.models__container__content__coupe`).hide();
    }

    $('[data-models]').removeClass('actived');
    $(e.currentTarget).addClass('actived');
    $(`.models__container__content__${target}`).fadeIn(`fast`);
    $(`.models__container__content-mobile .models__container__content__${target}`).slick('refresh');
  });

  function loadModels(model) {
    const template = `
    <div class="models__container__content__{{ type }}__model four columns" data-model="{{ id }}">
        <img alt="Modelo {{ type }}" src="{{ images.thumb }}" />
        <h4>{{ title }}</h4>
        <button class="view-more" data-model="{{ id }}">
            Conheça
        </button>
      </div>`;
      const html = Mustache.to_html(template, model);

      $(`.models__container__content__${model.type}`).append(html);
      
    }

    $(document).on('click', '[data-model]', (e) => {
      if (modalActive) {
        return;
      }

      const id = $(e.currentTarget).data('model');
      const modal = dataModels.find(item => item.id === id);

      $('.models__container__content').css('opacity', 0);

     $('body').css('overflow', 'hidden');

      openModal(modal);
    });

    $(document).on('click', '.models__container__modal .closeModal', () => {
      $('body, html').removeAttr('style');
     
        closeModal();
    });

    function openModal(modal) {
      const template = `
      <div class="models__container__modal__content">
          <div class="models__container__modal__content__gallery">
              <div class="models__container__modal__gallery__item">
                  {{#images.gallery}}
                    <img alt="Modelo {{ title }}"  src="{{.}}" />
                  {{/images.gallery}}
              </div>
              <div class="models__container__modal__gallery__content">
                  <div class="five columns">
                  <h4> {{ title }} </h4>
                  </div>
                  <div class="six columns">
                    <table class="specs" cellpadding="0" cellspacing="0">
                      {{ #specifications }}
                        <tr>
                          <td class="name">
                            {{{ text }}}
                          </td>
                          <td class="specs-result">
                            {{{ value }}}
                          </td>
                        </tr>
                      {{ /specifications }}

                      {{ #disclaimer }}
                        <tr>
                          <td colspan="2">
                            {{{ text }}}
                          </td>
                        </tr>
                      {{ /disclaimer }}
                    </table>
                  </div>
              </div>
          </div>
      </div>`;

      $('.nav-head').hide();
      $('.models__container__header').css('opacity', 0);
      $('.models').addClass('actived');
      const html = Mustache.to_html(template, modal);

      modalActive = true;

      /*dataLayer.push({
        tipo: 'Evento',
        category: 'classe-c-facelift',
        action: 'modelos',
        label: 'conheca-' + modal.title,
    });*/

    ga('send', 'event', 'classe-c-facelift', 'modelos', 'conheca-' + modal.title);

    $('body').css('overflow', 'hidden');

      $(html).insertAfter('.models__container__modal header');
      $('.models__container__modal').fadeIn('fast');

      $('.models__container__modal__gallery__item').slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        arrows: false,
        dots: true,
        arrows: true,
        infinite: false,
        cssEase: 'cubic-bezier(.25,.48,.56,.74)',
      });


    }

    function closeModal() {
      $('.models__container__modal').fadeOut('fast', () => {
        $('.nav-head').fadeIn();
        $('.models__container__modal__content').remove();
        modalActive = false;
        $('.models').removeClass('actived');
        $('.models__container__content, .models__container__header').css('opacity', 1);
      });
    }


    
  /***
   *      __  __              
   *     |  \/  |__ _ _ __ ___
   *     | |\/| / _` | '_ (_-<
   *     |_|  |_\__,_| .__/__/
   *                 |_|      
   */

  let dealersUfs;
  let dealersCities;
  let dealerships;

  loadUFS();

  $('.send').on('click', () => {
    setCenter(dealerships[0].lat, dealerships[0].lng, 16);
    //dealerInfos(dealersCities[0]);
    //console.log(dealerships[0]);
    dealerInfos(dealerships[0]);
  });

  function loadUFS() {
      $.ajax({
        url: "./js/dealers/dealers_ufs.json",
        dataType: 'json',
      })
      .then((response) => {
        let template;
        dealersUfs = response;
        $(window).trigger('load:ufs-form');

        dealersUfs.forEach((item) => {
          template += `<option value="${item.uf}"> ${item.uf} </option>`;
        });

        $('#estado').append(template);
        $('#concessionarias select').selectric({
          nativeOnMobile: false,
          preventWindowScroll: false,
          onChange: function(element, event) {
            const value = $(element).val();
            const isState = $(element).attr('id') === 'estado';
            const isCity = $(element).attr('id') === 'cidade';
            
            if (isState) 
            {
              //console.log(value, isState, isCity, dealersUfs, dealersCities);

              $('#concessionarias #cidade option:not(:first)').remove().selectric('refresh');
              $('#concessionarias #concess option:not(:first)').remove();
              $('#concessionarias #concess').attr('disabled', true).selectric('refresh');
              $('.send').addClass('disabled').attr('disabled', true);
              $('.selecionar-concessionarias').fadeOut();

              if (value) {
                loadCities(value);
              }
              return;
            }

            if (isCity) 
            {
              //console.log(value, isState, isCity, dealersUfs, dealersCities);

              $('#concessionarias #concess option:not(:first)').remove();
              $('#concessionarias #concess').selectric('refresh');
              $('.selecionar-concessionarias').fadeOut();

              if (value) {
                loadDealers(value);
              }
              return;
            }


            if (isState === false && isCity === false) 
            {
              
              dealerships = $.grep(dealersCities, function(element) 
              {
                 if(element.id == value)
                 {
                    return element;
                 }
              });

              $('.send').removeClass('disabled').removeAttr('disabled');
            }
          },
        });
      });
  };
  
  function loadCities(state) {
      $.ajax({
        url: `./js/dealers/dealers-${state.toLowerCase()}.json`,
        dataType: 'json',
      })
      .then((response) => {
        let template;
        dealersCities = response;

        let compare = function(a,b)
        {
          if (a.name < b.name) { return -1; }
          if (a.name > b.name) { return 1; }
          return 0;
        }

        let cities = [];
        let cids = [];
        dealersCities.forEach((item) => {
          window.lat = item.lat;
          window.lng = item.lng;

          item.counties.forEach((city) =>
          {
            if (cids.indexOf(city.id.toString()) < 0)
            {
              cids.push(city.id.toString());
              cities.push({ 'id': city.id, 'name': city.name });
            }
          })
        });

        cities.sort(compare);
        
        for(var k in cities)
        {
          template += `<option value="${cities[k].id}"> ${cities[k].name} </option>`;
        }

        $('#cidade').append(template);
        $('#concessionarias #cidade').removeAttr('disabled').selectric('refresh');
      });
  }

  function loadDealers(city_id) {
      let template;

      dealersCities.forEach
      (
        (item) =>
        {
          var exists = $.grep(item.counties, function(e){ return e.id == city_id; });
          if (exists.length > 0)
          {
            template += `<option value="${item.id}"> ${item.aliasname} </option>`;
          }
        }
      );

      $('#concess').append(template);
      $('#concessionarias #concess').removeAttr('disabled').selectric('refresh');
  }

  function initMap() {
    const Brazil = {lat: -15.7801, lng: -47.92926};
    window.map = new google.maps.Map(
        document.getElementById('map'), {zoom: 4, center: Brazil,
        styles: [
          {
              "featureType": "all",
              "elementType": "geometry.fill",
              "stylers": [
                  {
                      "weight": "2.00"
                  }
              ]
          },
          {
              "featureType": "all",
              "elementType": "geometry.stroke",
              "stylers": [
                  {
                      "color": "#9c9c9c"
                  }
              ]
          },
          {
              "featureType": "all",
              "elementType": "labels.text",
              "stylers": [
                  {
                      "visibility": "on"
                  }
              ]
          },
          {
              "featureType": "landscape",
              "elementType": "all",
              "stylers": [
                  {
                      "color": "#f2f2f2"
                  }
              ]
          },
          {
              "featureType": "landscape",
              "elementType": "geometry.fill",
              "stylers": [
                  {
                      "color": "#ffffff"
                  }
              ]
          },
          {
              "featureType": "landscape.man_made",
              "elementType": "geometry.fill",
              "stylers": [
                  {
                      "color": "#ffffff"
                  }
              ]
          },
          {
              "featureType": "poi",
              "elementType": "all",
              "stylers": [
                  {
                      "visibility": "off"
                  }
              ]
          },
          {
              "featureType": "road",
              "elementType": "all",
              "stylers": [
                  {
                      "saturation": -100
                  },
                  {
                      "lightness": 45
                  }
              ]
          },
          {
              "featureType": "road",
              "elementType": "geometry.fill",
              "stylers": [
                  {
                      "color": "#eeeeee"
                  }
              ]
          },
          {
              "featureType": "road",
              "elementType": "labels.text.fill",
              "stylers": [
                  {
                      "color": "#7b7b7b"
                  }
              ]
          },
          {
              "featureType": "road",
              "elementType": "labels.text.stroke",
              "stylers": [
                  {
                      "color": "#ffffff"
                  }
              ]
          },
          {
              "featureType": "road.highway",
              "elementType": "all",
              "stylers": [
                  {
                      "visibility": "simplified"
                  }
              ]
          },
          {
              "featureType": "road.arterial",
              "elementType": "labels.icon",
              "stylers": [
                  {
                      "visibility": "off"
                  }
              ]
          },
          {
              "featureType": "transit",
              "elementType": "all",
              "stylers": [
                  {
                      "visibility": "off"
                  }
              ]
          },
          {
              "featureType": "water",
              "elementType": "all",
              "stylers": [
                  {
                      "color": "#46bcec"
                  },
                  {
                      "visibility": "on"
                  }
              ]
          },
          {
              "featureType": "water",
              "elementType": "geometry.fill",
              "stylers": [
                  {
                      "color": "#e8eceb"
                  },
                  {
                      "visibility": "on"
                  }
              ]
          },
          {
              "featureType": "water",
              "elementType": "labels.text.fill",
              "stylers": [
                  {
                      "color": "#070707"
                  }
              ]
          },
          {
              "featureType": "water",
              "elementType": "labels.text.stroke",
              "stylers": [
                  {
                      "color": "#ffffff"
                  }
              ]
          }
      ]});
  }

  function setCenter(lat, lng, zoom) {
    if (!zoom) {
      zoom = 6
    }

    const latLng = new google.maps.LatLng(lat, lng);
    window.map = new google.maps.Map(
      document.getElementById('map'), {zoom: zoom, center: latLng,
      styles: [
        {
            "featureType": "all",
            "elementType": "geometry.fill",
            "stylers": [
                {
                    "weight": "2.00"
                }
            ]
        },
        {
            "featureType": "all",
            "elementType": "geometry.stroke",
            "stylers": [
                {
                    "color": "#9c9c9c"
                }
            ]
        },
        {
            "featureType": "all",
            "elementType": "labels.text",
            "stylers": [
                {
                    "visibility": "on"
                }
            ]
        },
        {
            "featureType": "landscape",
            "elementType": "all",
            "stylers": [
                {
                    "color": "#f2f2f2"
                }
            ]
        },
        {
            "featureType": "landscape",
            "elementType": "geometry.fill",
            "stylers": [
                {
                    "color": "#ffffff"
                }
            ]
        },
        {
            "featureType": "landscape.man_made",
            "elementType": "geometry.fill",
            "stylers": [
                {
                    "color": "#ffffff"
                }
            ]
        },
        {
            "featureType": "poi",
            "elementType": "all",
            "stylers": [
                {
                    "visibility": "off"
                }
            ]
        },
        {
            "featureType": "road",
            "elementType": "all",
            "stylers": [
                {
                    "saturation": -100
                },
                {
                    "lightness": 45
                }
            ]
        },
        {
            "featureType": "road",
            "elementType": "geometry.fill",
            "stylers": [
                {
                    "color": "#eeeeee"
                }
            ]
        },
        {
            "featureType": "road",
            "elementType": "labels.text.fill",
            "stylers": [
                {
                    "color": "#7b7b7b"
                }
            ]
        },
        {
            "featureType": "road",
            "elementType": "labels.text.stroke",
            "stylers": [
                {
                    "color": "#ffffff"
                }
            ]
        },
        {
            "featureType": "road.highway",
            "elementType": "all",
            "stylers": [
                {
                    "visibility": "simplified"
                }
            ]
        },
        {
            "featureType": "road.arterial",
            "elementType": "labels.icon",
            "stylers": [
                {
                    "visibility": "off"
                }
            ]
        },
        {
            "featureType": "transit",
            "elementType": "all",
            "stylers": [
                {
                    "visibility": "off"
                }
            ]
        },
        {
            "featureType": "water",
            "elementType": "all",
            "stylers": [
                {
                    "color": "#46bcec"
                },
                {
                    "visibility": "on"
                }
            ]
        },
        {
            "featureType": "water",
            "elementType": "geometry.fill",
            "stylers": [
                {
                    "color": "#e8eceb"
                },
                {
                    "visibility": "on"
                }
            ]
        },
        {
            "featureType": "water",
            "elementType": "labels.text.fill",
            "stylers": [
                {
                    "color": "#070707"
                }
            ]
        },
        {
            "featureType": "water",
            "elementType": "labels.text.stroke",
            "stylers": [
                {
                    "color": "#ffffff"
                }
            ]
        }
    ] });

    window.marker = new google.maps.Marker({
      position: latLng,
      map: window.map,
    }); 
        
  };

  function dealerInfos(dealer) {
    const title = $('.selecionar-concessionarias .title');
    const state = $('.selecionar-concessionarias .state');
    const address = $('.selecionar-concessionarias .address');
    const telephone = $('.selecionar-concessionarias h6');

    title.html(dealer.name);
    state.html(dealer.city);
    address.html(`${dealer.address}, ${dealer.neighborhood}`)
    telephone.find('a').attr(`tel:${dealer.phone}`);
    telephone.find('a').html(dealer.phone);

    /*dataLayer.push({
        tipo: 'Evento',
        category: 'classe-c-facelift',
        action: 'galeria',
        label: 'procurar-' + dealer.uf + '-' + dealer.city + '-' + dealer.name,
    });*/

    ga('send', 'event', 'classe-c-facelift', 'galeria', 'procurar-' + dealer.uf + '-' + dealer.city + '-' + dealer.name);

    $('.selecionar-concessionarias').fadeIn();
  }

  
  /************************** FORM **************************************/

  let template; 

  $(window).on('load:ufs-form', loadUFSForm);

  $('.form__ico, .form__close').on('click', () => {
    $('.form').toggleClass('actived');
  });

  $('#lead select').selectric({
    nativeOnMobile: false,
    openOnFocus: false,
    onSelect: function(element, event) {
      const value = $(element).val();
      const isState = $(element).attr('id') === 'form_estado';
      const isCity = $(element).attr('id') === 'form_cidade';
      formActiveOnLoad = false;
  
      if (isState) {
        $('#form_cidade option:not(:first)').remove().selectric('refresh');
        $('#form_concessionaria option:not(:first)').remove();
        $('#form_concessionaria').attr('disabled', true).selectric('refresh');
        $('.send').addClass('disabled').attr('disabled', true);
        $('.selecionar-concessionarias').fadeOut();

        if (value) {
          loadCitiesForm(value);
        }
        return;
      }

      if (isCity) {
        $('#form_concessionaria option:not(:first)').remove();
        $('#form_concessionaria').selectric('refresh');

        if (value) {
          loadDealersForm(value);
        }
        return;
      }

      $('.send').removeClass('disabled').removeAttr('disabled');

    },
  });


  function loadUFSForm() {
    dealersUfs.forEach((item) => {
      template += `<option value="${item.uf}"> ${item.uf} </option>`;
    });
    
    $('#lead #form_estado').append(template).selectric('refresh');
    
  }

  function loadCitiesForm(state) {
    $.ajax({
      url: `./js/dealers/dealers-${state.toLowerCase()}.json`,
      dataType: 'json',
    })
    .then((response) => {
      let template;
      dealersCities = response;

      let compare = function(a,b) { if (a.name < b.name) { return -1; } if (a.name > b.name) { return 1; } return 0; }
      let cities = [];
      let cids = [];
      dealersCities.forEach((item) =>
      {
        item.counties.forEach((city) =>
        {
          if (cids.indexOf(city.id.toString()) < 0)
          {
            cids.push(city.id.toString());
            cities.push({ 'id': city.id, 'name': city.name });
          }
        })
      });

      cities.sort(compare);
      
      for(var k in cities)
      {
        template += `<option value="${cities[k].id}"> ${cities[k].name} </option>`;
      }

      $('#form_cidade').append(template);
      $('#form_cidade').removeAttr('disabled').selectric('refresh');
    });
}

  function loadDealersForm(city_id) {
    let template;

    dealersCities.forEach
    (
      (item) =>
      {
        var exists = $.grep(item.counties, function(e){ return e.id == city_id; });
        if (exists.length > 0)
        {
          template += `<option value="${item.id}"> ${item.aliasname} </option>`;
        }
      }
    );

    $('#form_concessionaria').append(template);
    $('#form_concessionaria').removeAttr('disabled').selectric('refresh');
  }

  $("#lead").validate({
    onkeyup: false,
    onclick: false,
    onfocusout: false,
    // Specify validation rules
    rules: {
      // The key name on the left side is the name attribute
      // of an input field. Validation rules are defined
      // on the right side
      'form_nome': "required",
      'form_email': {
        required: true,
        email: true,
      },
      'form_telefone': {
        required: true,
        minlength: 14,
      },
      'form_termos': 'required',
      'form_assunto': 'required',
      'form_estado': 'required',
      'form_cidade': 'required',
      'form_concessionaria': 'required',
    },
    errorPlacement: function(error, element) { 
        const isSelectric = $(element).parents('div').hasClass('selectric-hide-select');
        if (isSelectric) {
            $(element).parent('.selectric-hide-select').next('.selectric').addClass('error');
        }
    },
    // Make sure the form is submitted to the destination defined
    // in the "action" attribute of the form when valid
    submitHandler: function(form, event) {
      if (grecaptcha.getResponse() == ''){
        $('.recaptcha').addClass('error');
        return;
      }

      window.mercedes_leads.form_leads.sendForm();
    }
  });

  $(window).on('scroll', () => {
    if (formActiveOnLoad) {
      $('.form').removeClass('actived');
      formActiveOnLoad = false;
    }
  });
});
