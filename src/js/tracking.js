$(document).on('ready', () => {
    let timer = null;
    let timeSpent = [];
    let player = null;
    let video25 = false;
    let video50 = false;
    let video75 = false;
    let video100 = false;
    let scroll25 = false;
    let scroll50 = false;
    let scroll75 = false;
    let scroll100 = false;

    const Tracking = {
        init() {
            const self = this;
            var tag = document.createElement('script');
            var firstScript = document.getElementsByTagName('script')[0];
        
            tag.src = 'https://www.youtube.com/iframe_api';
            firstScript.parentNode.insertBefore(tag, firstScript);

            window.onYouTubeIframeAPIReady = function () {
                player = new YT.Player('player', {
                    height: '100%',
                    width: '100%',
                    videoId: 'xpa01bmVQ8Q',
                    events: {
                    'onStateChange': self.onPlayerStateChange
                    },
                    playerVars: {
                        rel: 0,
                        showinfo: false,
                        modestbranding: false,
                    }
                });
            }

            this.triggers();
        },
        triggers() {
            $('.nav-head [data-scroll]').on('click', (e) => {
                const text = $(e.currentTarget).attr('data-anchor');

                /*dataLayer.push({
                    tipo: 'Evento',
                    category: 'classe-c-facelift',
                    action: 'menu',
                    label: text
                });*/

                ga('send', 'event', 'classe-c-facelift', 'menu', text);
            });

            $('.form [name="form_assunto"]').change((e) => {
                const value = $(e.currentTarget).val();

                /*dataLayer.push({
                    tipo: 'Evento',
                    category: 'classe-c-facelift',
                    action: 'formulario',
                    label: `assunto-${value}`,
                });*/

                ga('send', 'event', 'classe-c-facelift', 'formulario', `assunto-${value}`);
            });

            $('.form [name="form_estado"]').change((e) => {
                const value = $(e.currentTarget).val();

                /*dataLayer.push({
                    tipo: 'Evento',
                    category: 'classe-c-facelift',
                    action: 'formulario',
                    label: `estado-${value}`,
                });*/

                ga('send', 'event', 'classe-c-facelift', 'formulario', `estado-${value}`);
            });

            $('.form [name="form_cidade"]').change((e) => {
                const value = $(e.currentTarget).parent('div').next('.selectric').text().trim().replace(' ', '-').replace(' ▾', '');

                /*dataLayer.push({
                    tipo: 'Evento',
                    category: 'classe-c-facelift',
                    action: 'formulario',
                    label: `cidade-${value}`,
                });*/

                ga('send', 'event', 'classe-c-facelift', 'formulario', `cidade-${value}`);
            });

            $('.form [name="form_concessionaria"]').change((e) => {
                const value = $(e.currentTarget).parent('div').next('.selectric').text().trim().replace('-', ' ').replace(' ', '-').replace(' ▾', '');

                /*dataLayer.push({
                    tipo: 'Evento',
                    category: 'classe-c-facelift',
                    action: 'formulario',
                    label: `concessionaria-${value}`,
                });*/

                ga('send', 'event', 'classe-c-facelift', 'formulario', `concessionaria-${value}`);
            });

            $('.form [name="form_nome"], .form [name="form_telefone"], .form [name="form_email"], .form [name="form_comentario"]').blur((e) => {
                const type = $(e.currentTarget).attr('name').replace('form_', '');

                /*dataLayer.push({
                    tipo: 'Evento',
                    category: 'classe-c-facelift',
                    action: 'formulario',
                    label: type,
                });*/

                ga('send', 'event', 'classe-c-facelift', 'formulario', type);
            });

            $('.hotspot__container__nav__external, hotspot__container__nav__external--mobile').click((e) => {
                /*dataLayer.push({
                    tipo: 'Evento',
                    category: 'classe-c-facelift',
                    action: 'novidades',
                    label: 'exterior',
                });*/

                ga('send', 'event', 'classe-c-facelift', 'novidades', 'exterior');
            });

            $('.hotspot__container__nav__internal, hotspot__container__nav__internal--mobile').click((e) => {
                /*dataLayer.push({
                    tipo: 'Evento',
                    category: 'classe-c-facelift',
                    action: 'novidades',
                    label: 'interior',
                });*/

                ga('send', 'event', 'classe-c-facelift', 'novidades', 'interior');
            });

            $('.hotspot .hotspot-item').click((e) => {
                const title = $(e.currentTarget).find('.tooltip h4.title').text().toLowerCase().replace(' ', '-');
                /*dataLayer.push({
                    tipo: 'Evento',
                    category: 'classe-c-facelift',
                    action: 'novidades',
                    label: 'clique-' + title,
                });*/

                ga('send', 'event', 'classe-c-facelift', 'novidades', 'clique-' + title);
            });

            $('.gallery .slick-next, .gallery .slick-prev').click((e) => {
                /*dataLayer.push({
                    tipo: 'Evento',
                    category: 'classe-c-facelift',
                    action: 'galeria',
                    label: 'navegacao',
                });*/

                ga('send', 'event', 'classe-c-facelift', 'galeria', 'navegacao');
            });

            $('.gallery .zoom').click((e) => {
                /*dataLayer.push({
                    tipo: 'Evento',
                    category: 'classe-c-facelift',
                    action: 'galeria',
                    label: 'zoom',
                });*/

                ga('send', 'event', 'classe-c-facelift', 'galeria', 'zoom');
            });

            $('[data-models]').click((e) => {
                const value = $(e.currentTarget).data('models');

                /*dataLayer.push({
                    tipo: 'Evento',
                    category: 'classe-c-facelift',
                    action: 'modelos',
                    label: 'menu-' + value,
                });*/

                ga('send', 'event', 'classe-c-facelift', 'modelos', 'menu-' + value);
            });


            $(window).scroll(function(e){
                var scrollTop = $(window).scrollTop();
                var docHeight = $(document).height();
                var winHeight = $(window).height();
                var scrollPercent = (scrollTop) / (docHeight - winHeight);
                var scrollPercentRounded = Math.round(scrollPercent*100);

                if (scrollPercentRounded === 25 && !scroll25) {
                    scroll25 = true;

                    /*dataLayer.push({
                        tipo: 'Evento',
                        category: 'classe-c-facelift',
                        action: 'scroll',
                        label: 'scroll-25',
                    });*/

                    ga('send', 'event', 'classe-c-facelift', 'scroll', 'scroll-25');
                }

                if (scrollPercentRounded === 50 && !scroll50) {
                    scroll50 = true;

                    /*dataLayer.push({
                        tipo: 'Evento',
                        category: 'classe-c-facelift',
                        action: 'scroll',
                        label: 'scroll-50',
                    });*/

                    ga('send', 'event', 'classe-c-facelift', 'scroll', 'scroll-50');
                }

                if (scrollPercentRounded === 75 && !scroll75) {
                    scroll75 = true;

                    /*dataLayer.push({
                        tipo: 'Evento',
                        category: 'classe-c-facelift',
                        action: 'scroll',
                        label: 'scroll-75',
                    });*/

                    ga('send', 'event', 'classe-c-facelift', 'scroll', 'scroll-75');
                }

                if (scrollPercentRounded === 100 && !scroll100 ) {
                    scroll100 = true;

                    /*dataLayer.push({
                        tipo: 'Evento',
                        category: 'classe-c-facelift',
                        action: 'scroll',
                        label: 'scroll-100',
                    });*/

                    ga('send', 'event', 'classe-c-facelift', 'scroll', 'scroll-100');
                }
            });

        },
        onPlayerStateChange: function onPlayerStateChange(event) {
            if(event.data === 1) { // Started playing
                /*dataLayer.push({
                    tipo: 'Evento',
                    category: 'classe-c-facelift',
                    action: 'conheça',
                    label: 'play',
                });*/

                ga('send', 'event', 'classe-c-facelift', 'conheça', 'play');

                if(!timeSpent.length){
                    for(var i=0, l=parseInt(player.getDuration()); i<l; i++) timeSpent.push(false);
                }
                timer = setInterval(Tracking.record, 100);
            } else {
                clearInterval(timer);
            }
        },
        record() {
            timeSpent[ parseInt(player.getCurrentTime()) ] = true;

            if (Tracking.getPercentage() === 27 && !video25) {
                video25 = true;
                /*dataLayer.push({
                    tipo: 'Evento',
                    category: 'classe-c-facelift',
                    action: 'conheça',
                    label: 'play-25%',
                });*/

                ga('send', 'event', 'classe-c-facelift', 'conheça', 'play-25%');
            }

            if (Tracking.getPercentage() === 50 && !video50) {
                video50 = true;
                /*dataLayer.push({
                    tipo: 'Evento',
                    category: 'classe-c-facelift',
                    action: 'conheça',
                    label: 'play-50%',
                });*/

                ga('send', 'event', 'classe-c-facelift', 'conheça', 'play-50%');
            }

            if (Tracking.getPercentage() === 77 && !video75) {
                video75 = true;
                /*dataLayer.push({
                    tipo: 'Evento',
                    category: 'classe-c-facelift',
                    action: 'conheça',
                    label: 'play-75%',
                });*/

                ga('send', 'event', 'classe-c-facelift', 'conheça', 'play-75%');
            }

            if (Tracking.getPercentage() === 100 && !video100) {
                video100 = true;
                /*dataLayer.push({
                    tipo: 'Evento',
                    category: 'classe-c-facelift',
                    action: 'conheça',
                    label: 'play-100%',
                });*/

                ga('send', 'event', 'classe-c-facelift', 'conheça', 'play-100%');
            }
        },
        getPercentage() {
            var percent = 0;
            for(var i=0, l=timeSpent.length; i<l; i++){
                if(timeSpent[i]) percent++;
            }
            percent = Math.round(percent / timeSpent.length * 100);
            return parseInt(percent);
        },
    };

    Tracking.init();
});